'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

app.get('/', (req, res) => {
    res.send('Post user preferences: name and email');
});

function hasValidRequest(requestBody) {
  const keysArray = Object.keys(requestBody)
  if (keysArray.length === 2 && keysArray.includes('email') && keysArray.includes('name')) {
    return true
  }
  return false
}

class User {
  constructor(user) {
    const userKeys = Object.keys(user)
    console.log(userKeys)

    if (!(userKeys.length === 2 && userKeys.includes('email') && userKeys.includes('name'))) {
      throw new Error('Please provide only email and name when initialising user')
    }

    const { email, name } = user

    this.email = this.validateEmail(email)
    this.name = this.validateName(name)
    this.id = this.authUserId()
    this.role = 'user'

    Object.freeze(this)
  }

  validateEmail(email) {
    const MAX_EMAIL_LENGTH = 400
    const SIMPLE_EMAIL_REGEX = /.+\@.+\..+/

    if (typeof email === 'string'
      && email.length <= MAX_EMAIL_LENGTH
      && email.length > 0
      && SIMPLE_EMAIL_REGEX.test(email)) {
      return email
    }

    throw new Error('Please provide a valid email address')
  }

  validateName(name) {
    if (typeof name === 'string'
      && name.length > 0
      && name.length < 150) {
      return name
    }

    throw new Error('Please provide a valid name')
  }

  authUserId() {
    return 2
  }
}

// curl -X POST http://localhost:8080 -H 'Content-Type: application/json' -d '{"name": "foo", "email":"foo@bar.com" }'
app.post('/', (req, res) => {
    const { email, name } = req.body || {}

    if (typeof email === 'undefined' || typeof name === 'undefined') {
      return res.status(400).send('Please provide both email and name');
    }

    let user

    try {
      user = new User(req.body)
    } catch (e) {
      return res.status(400).send(e.message);
    }

    console.log(user)

    res.send('Your preferences have been successfully saved');
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
